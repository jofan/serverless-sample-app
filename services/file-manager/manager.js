'use strict';

module.exports.retrieveFile = (event, context, cb) => cb(null,
  { message: 'I got the file. Yay!', event }
);

// Gets event from S3 bucket
module.exports.confirmFile = (event, context, cb) => {
  var bucketName = "";
  var records = event.Records || [];
  if (records.length) {
    if (records[0].s3) bucketName = records[0].s3.bucket.name;
  }
  if (bucketName) {
    cb(null, { message: 'File uploaded successfully to ' + bucketName });
  }
  else {
    cb(null, { message: 'Nothing uploaded to bucket' });
  }
};

module.exports.confirmRemoveFile = (event, context, cb) => {
  var bucketName = "";
  var records = event.Records || [];
  if (records.length) {
    if (records[0].s3) bucketName = records[0].s3.bucket.name;
  }
  if (bucketName) {
    cb(null, { message: 'File removed successfully from ' + bucketName });
  }
  else {
    cb(null, { message: 'Nothing removed from bucket' });
  }
};

module.exports.fileRequested = (event, context, cb) => cb(null,
  { message: 'Somebody requested a file. Nice!', event }
);
