'use strict';

module.exports.handler = (event, context, cb) => cb(null,
  { message: 'Somebody requested a file. Nice!', event }
);
