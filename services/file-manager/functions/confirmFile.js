'use strict';

// Gets event from S3 bucket
module.exports.handler = (event, context, cb) => {
  var bucketName = "";
  var records = event.Records || [];
  if (records.length) {
    if (records[0].s3) bucketName = records[0].s3.bucket.name;
  }
  if (bucketName) {
    cb(null, { message: 'File uploaded successfully to ' + bucketName });
  }
  else {
    cb(null, { message: 'Nothing uploaded to bucket' });
  }
};
