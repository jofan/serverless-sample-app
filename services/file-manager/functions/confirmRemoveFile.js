'use strict';

module.exports.handler = (event, context, cb) => {
  var bucketName = "";
  var records = event.Records || [];
  if (records.length) {
    if (records[0].s3) bucketName = records[0].s3.bucket.name;
  }
  if (bucketName) {
    cb(null, { message: 'File removed successfully from ' + bucketName });
  }
  else {
    cb(null, { message: 'Nothing removed from bucket' });
  }
};
