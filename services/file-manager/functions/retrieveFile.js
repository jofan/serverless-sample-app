'use strict';

module.exports.handler = (event, context, cb) => {
  var response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'I got the file. Yay!',
      input: event
    })
  };
  cb(null, response);
};
