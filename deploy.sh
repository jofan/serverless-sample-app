function deploy {
  (
    cd $1;\
    rm .env;\
    echo STAGE=$2 >> .env;\
    echo REGION=$3 >> .env;\
    echo "Deploying $1 to stage $2 in region $3..."
    sls deploy --stage $2 --region $3 --verbose;\
  )
}

for dir in ./integrations/*; do
  if [[ -d "$dir" ]]; then
    deploy $dir $1 $2
  fi
done

for dir in ./services/*; do
  if [[ -d "$dir" ]]; then
    deploy $dir $1 $2
  fi
done
