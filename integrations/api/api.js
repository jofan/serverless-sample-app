'use strict';

require('dotenv').config(); // Populate process.env
const lambda = require('aws-lambda-invoke');

module.exports.retrieveFile = (event, context, cb) => {
  const query = event.query;
  const stage = process.env.STAGE;

  const functionName = `${stage}-file-manager-retrieveFile`;

  lambda.invoke(functionName, { query }, false)
  .then((result) => {
    cb(null, result);
  })
  .catch((error) => {
    cb(JSON.stringify(error), null);
  });
};
