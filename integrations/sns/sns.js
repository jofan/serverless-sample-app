'use strict';

require('dotenv').config(); // Populate process.env
const lambda = require('aws-lambda-invoke');

module.exports.fileRequested = (event, context, cb) => {
  const stage = process.env.STAGE;

  const functionName = `${stage}-file-manager-fileRequested`;

  lambda.invoke(functionName, { event }, false)
  .then((result) => {
    cb(null, result);
  })
  .catch((error) => {
    cb(new Error(error), null);
  });
};
