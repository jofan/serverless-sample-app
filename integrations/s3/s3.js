'use strict';

require('dotenv').config(); // Populate process.env
const lambda = require('aws-lambda-invoke');

// FUNCTIONS

module.exports.confirmFile = (event, context, cb) => {
  invoke("file-manager-confirmFile", event, cb);
};

module.exports.confirmRemoveFile = (event, context, cb) => {
  invoke("file-manager-confirmRemoveFile", event, cb);
};

// INVOKE SERVICE LAMBDA

const invoke = (name, event, cb) => {
  const stage = process.env.STAGE;

  const functionName = `${stage}-${name}`;

  // From S3 we get an object like { "event": { "Records": [] } }
  var data = event.event || event;

  lambda.invoke(functionName, { data }, false)
  .then((result) => {
    cb(null, result);
  })
  .catch((error) => {
    console.log("S3 invokation failed", error);
    cb(new Error(error), null);
  });
}
